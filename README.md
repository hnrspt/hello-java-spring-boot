# hello-java-spring-boot

Sample CI/CD Pipeline for Openshift deployment of a simple Spring Boot application
 
 
# Prepare CI/CD Tools
 
# OCP Dev
oc new-project cicd --display-name="CI/CD Tools"  
oc new-app jenkins-persistent  
oc new-app nexus3-persistent  
oc new-project <project-name> --display-name="CI/CD - Dev" 
 
# OCP Prod
oc new-project <project-name> --display-name="CI/CD - Prod"  
oc create sa jenkins 
 
 
# Grant Jenkins Access to Projects
 
# OCP Dev
oc policy add-role-to-group edit system:serviceaccounts:cicd -n <project-name> 
 
# OCP Prod
oc policy add-role-to-group edit system:serviceaccounts:<project-name> -n <project-name> 
 
 
# Create new Build, DeploymentConfig, Override Trigger
 
# OCP Dev
oc new-build --name=<app-name> -n cicd --binary=true  
oc new-app image-registry.openshift-image-registry.svc:5000/<project-name>/<app-name>:latest \  
--allow-missing-images --as-deployment-config -n <project-name>  
oc set triggers dc -l app=<app-name> --containers=<app-name> --manual -n <project-name> 
 
# OCP Prod
oc new-app image-registry.openshift-image-registry.svc:5000/<project-name>/<app-name>:latest \  
--allow-missing-images --as-deployment-config -n <project-name>  
oc set triggers dc -l app=<app-name> --containers=<app-name> --manual -n <project-name> 
  
