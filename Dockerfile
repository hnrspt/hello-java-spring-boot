# Build the application first using Maven
FROM image-registry.openshift-image-registry.svc:5000/openshift/maven:latest as build
WORKDIR /app
COPY . .
USER root
RUN mvn deploy -s settings.xml

# Inject the JAR file into a new container to keep the file small
FROM image-registry.openshift-image-registry.svc:5000/openshift/ubi8-openjdk-11:1.3
WORKDIR /app
COPY --from=build /app/target/hello-java-spring-boot-*.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["sh", "-c"]
CMD ["java -jar app.jar"]
