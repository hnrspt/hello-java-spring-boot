// Based on:
// https://raw.githubusercontent.com/redhat-cop/container-pipelines/master/basic-spring-boot/Jenkinsfile

library identifier: "pipeline-library@v1.6",
  retriever: modernSCM(
    [
      $class: "GitSCMSource",
      remote: "https://github.com/redhat-cop/pipeline-library.git"
    ]
  )

// The name you want to give your Spring Boot application
// Each resource related to your app will be given this name
appName = "hello-java-spring-boot"
ocpDevUrl = "image-registry.openshift-image-registry.svc:5000"
ocpProdUrl = "default-route-openshift-image-registry.apps.cluster-ba7e.ba7e.sandbox1231.opentlc.com"

pipeline {
  // Use the 'maven' Jenkins agent image which is provided with OpenShift 
  agent {
    label "maven"
  }
  //parameters {
    //credentials credentialType: 'com.openshift.jenkins.plugins.OpenShiftTokenCredentials', name: 'ocp_stage_cred', defaultValue: '', description: '', required: true
  //}
  stages {
    stage("Checkout") {
      steps {
        checkout scm
      }
    }
    stage("Docker Build") {
      steps {
        // This uploads your application's source code and performs a binary build in OpenShift
        // This is a step defined in the shared library (see the top for the URL)
        // (Or you could invoke this step using 'oc' commands!)
        binaryBuild(buildConfigName: appName, buildFromPath: ".", projectName: "cicd")
      }
    }
    stage('Deploy to DEV') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("cicd/${appName}:latest", "${appName}/${appName}:latest")
            openshift.withProject("${appName}") {
              openshift.selector("dc", appName).rollout().latest()
            }
          }
        }
      }
    }
    stage('Promote to PROD?') {
      // Jenkins agent skopeo
      agent {
        label 'skopeo'
      }
      steps {
        timeout(time: 60, unit: 'MINUTES') {
          input message: "Promote to PROD?", ok: "Promote"
        }
        script {
          openshift.withCluster() {
            withCredentials([string(credentialsId: "ocp-dev-cred", variable: "ocp_dev_cred")]) {
              withCredentials([string(credentialsId: "ocp-prod-cred", variable: "ocp_prod_cred")]) {
                sh "skopeo copy docker://${ocpDevUrl}/${appName}/${appName}:latest docker://${ocpProdUrl}/${appName}/${appName}:latest --src-registry-token $ocp_dev_cred --dest-registry-token $ocp_prod_cred --src-tls-verify=false --dest-tls-verify=false --debug"
              }
            }
          }
        }
      }
    }
    stage('Deploy to PROD') {
      steps {
        script {          
          openshift.withCluster("prod") {            
            openshift.withProject("${appName}") {
              withCredentials([string(credentialsId: 'ocp-prod-cred', variable: 'ocp_prod_cred')]) {
                openshift.withCredentials("${ocp_prod_cred}") {
                  openshift.selector("dc", appName).rollout().latest()
                }
              }
            }
          }
        }
      }
    }
  }
}